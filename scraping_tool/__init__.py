from selenium import webdriver
from selenium.webdriver.common.by import By
from config import DRIVERS_PATH, URL
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# initialize browser object with custom settings
browser = webdriver.Chrome(DRIVERS_PATH)
browser.implicitly_wait(15)


def get_landline_prices(*args):
    """
    Gets and returns landline prices for specified countries and returns them as a dictionary.

    Firstly, I wanted to assign elements found by find_element_by_xpath to variables but then I saw that actually
    ids are pretty clear and it's easy to gain information about the element only by its xpath.
    """
    landline_rates = {}

    for arg in args:
        click_element_by_xpath("//a[@id='showAllCountries']")
        click_element_by_xpath("//div[@id='countryLinkContainer']//a[text()='{}']".format(arg))
        wait_until_element_is_not_visible("//div[@id='paymonthlyTariffPlan']")
        click_element_by_xpath("//a[@id='paymonthly']")
        landline_single_rate = browser.find_element_by_xpath("//div[@id='paymonthlyTariffPlan']//table"
                                                      "[@id='standardRatesTable']//td[text()='Landline']/../td[2]")
        landline_rates[arg] = landline_single_rate.get_attribute('innerHTML')
    return landline_rates


def wait_until_element_is_not_visible(xpath):
    """
    Waits until element located by xpath is not visible.
    """
    WebDriverWait(browser, 10).until(
        EC.invisibility_of_element_located((By.XPATH, xpath))
    )


def click_element_by_xpath(xpath):
    """
    Waits until element located by xpath is clickable and then performs a click.
    It's possible to change default (5s) timeout by passing it as a parameter.
    """
    element = browser.find_element_by_xpath(xpath)
    element.click()


def main():
    browser.get(URL)
    prices = get_landline_prices('Canada', 'Germany', 'Iceland', 'Pakistan', 'Singapore', 'South Africa')

    for key in prices.keys():
        print key + ' - ' + prices[key]

if __name__ == "__main__":
    main()
